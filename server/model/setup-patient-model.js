const Sequelize = require('sequelize')

class Patient extends Sequelize.Model { }

module.exports = (sequelizeConnection) => {
  return Patient.init({
    emailAddress: {
      type: Sequelize.STRING,
      allowNull: false
    },
    bucketKey: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }, {
    sequelize: sequelizeConnection,
    modelName: 'patient'
  })
}