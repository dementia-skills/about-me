const express = require('express')
const consola = require('consola')

const path = require('path')
const fs = require('fs')
const AWS = require('aws-sdk')

const Sequelize = require('sequelize')
const setupPatientModel = require('./model/setup-patient-model')

const createPatientInfo = require('./api/create-patient')

const { Nuxt, Builder } = require('nuxt')

const app = express()

const PROJECT_PATH = path.join(__dirname, '..')
const credentialsConfigPath = path.join(PROJECT_PATH, 'aws-credentials.json')
const s3ConfigPath  = path.join(PROJECT_PATH, 's3-config.json')
const developmentDBPath = path.join(PROJECT_PATH, 'test-db.sqlite')

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

function setupAWS() {
  const credentialsConfigPath = path.join(__dirname, '..', 'aws-credentials.json')
  const s3ConfigPath  = path.join(__dirname, '..', 's3-config.json')

  try {
    AWS.config.loadFromPath(credentialsConfigPath)

    return new AWS.S3({ params: JSON.parse(fs.readFileSync(s3ConfigPath)) })
  } catch(e) {
    console.error('Could not find or load aws-config.json file at the root of the project')
    console.error(e)
  }
}

async function setupDB() {
  const sequelize = new Sequelize(process.env.DATABASE_URL || {
    dialect: 'sqlite',
    storage: developmentDBPath
  });
  const patientModel = setupPatientModel(sequelize)

  try {
    await sequelize.authenticate();
    console.log('Database connection is successfully setup')
  } catch(e) {
    console.error('Could not connect to the database server')
    console.error(e)
    return e
  }

  try {
    await sequelize.sync();
  } catch(e) {
    console.error('Patient table could not be synchronized')
    console.error(e)
    return e
  }

  return {
    Patient: patientModel
  }
}

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)
  const s3Client = setupAWS();

  const { host, port } = nuxt.options.server

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }

  const db = await setupDB();

  app.use(express.json())

  // API routes
  app.post('/api/patients/', createPatientInfo(s3Client, db))

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
setupAWS();
start()
