module.exports = (s3, db) => async (req, response) => {
  const patientData = req.body
  const { emailAddress } = patientData
  const bucketKey = emailAddress
  const patientExists = (await db.Patient.count({ where: { emailAddress } })) > 0

  if (patientExists) {
    return response.status(400).json({ message: 'A patient with this email address already exists' })
  }

  try {
    await uploadPatientData(s3, bucketKey, patientData)
  } catch(e) {
    console.error(e)
    return response.status(500).json({ message: 'Patient could not be created' });
  }

  try {
    await createPatientRecord(db, { bucketKey, emailAddress })
    response.status(201).json({ message: 'Patient was created successfully' });
  } catch(e) {
    console.error(e)
    return response.status(500).json({ message: 'Patient could not be created' })
  }
}

const createPatientRecord = ({ Patient }, { bucketKey, emailAddress }) => {
  return Patient.create({ bucketKey, emailAddress })
}

const uploadPatientData = (s3, bucketKey, patientData) => {
  return new Promise((resolve, reject) => {
    s3.putObject({
      Body: JSON.stringify(patientData),
      Key: bucketKey
    })
    .on('success', resolve)
    .on('error', reject)
    .send()
  })
}