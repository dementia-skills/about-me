# about-me

> Capture basic information for Alexa

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## Setting up AWS credentials for S3

- Read how to obtain your credentials [here](https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/getting-your-credentials.html)

- Create a file named `aws-credentials.json` at the root of the project with the following structure and fill the values in:

```json
{
  "region": "",
  "accessKeyId": "",
  "secretAccessKey": ""
}
```

- Create a file named `s3-config.json` at the root of the project with the following structure and fill the values in:

```json
{
  "Bucket": ""
}
```
